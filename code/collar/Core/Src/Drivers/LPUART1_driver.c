/*
 * LPUART1_driver.c
 *
 *  Author: AyRobi
 */

#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_exti.h"
#include "stm32l4xx_ll_cortex.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_lpuart.h"

#include "debug.h"

#include "Drivers/LPUART1_driver.h"

#include "defines.h"

static USART_TypeDef *UART = LPUART1;

int LPUART1_Init(void)
{
	if (LL_LPUART_IsEnabled(UART))
		return ALREADY_IN_USE_ERROR;

	LL_LPUART_InitTypeDef LPUART_Init = {
			.BaudRate = 9600,
			.DataWidth = LL_LPUART_DATAWIDTH_8B,
			.StopBits = LL_LPUART_STOPBITS_1,
			.Parity = LL_LPUART_PARITY_NONE,
			.TransferDirection = LL_LPUART_DIRECTION_TX_RX,
			.HardwareFlowControl = LL_LPUART_HWCONTROL_NONE
	};

	LL_GPIO_InitTypeDef GPIO_Init = {
			.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1,
			.Mode = LL_GPIO_MODE_ALTERNATE,
			.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
			.OutputType = LL_GPIO_OUTPUT_PUSHPULL,
			.Pull = LL_GPIO_PULL_NO,
			.Alternate = LL_GPIO_AF_8
	};

	LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_LPUART1);
	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC);

	LL_GPIO_Init(GPIOC, &GPIO_Init);
	LL_LPUART_Init(UART, &LPUART_Init);

	LL_LPUART_EnableIT_RXNE(UART);

	NVIC_SetPriority(LPUART1_IRQn, 15);
	NVIC_EnableIRQ(LPUART1_IRQn);

	LL_LPUART_Enable(UART);

	return OK;
}

int LPUART1_Send(uint8_t *data, uint32_t len)
{
	if (!LL_LPUART_IsEnabled(UART))
		return NOT_INITIALIZED_ERROR;

	for (uint32_t i = 0; i < len; ++i) {
		while (!LL_LPUART_IsActiveFlag_TXE(UART));
		LL_LPUART_TransmitData8(UART, data[i]);
	}

	return len;
}

void LPUART1_IRQHandler(void)
{
	if (LL_LPUART_IsActiveFlag_RXNE(UART)) {
		uint8_t data = LL_LPUART_ReceiveData8(UART);
		debug("%c", data);
	}

	if (LL_LPUART_IsActiveFlag_ORE(UART))
		LL_LPUART_ClearFlag_ORE(UART);
}
