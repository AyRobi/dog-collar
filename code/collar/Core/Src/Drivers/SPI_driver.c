/*
 * SPI_driver.c
 *
 *  Author: AyRobi
 */


#include "Drivers/SPI_driver.h"

#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_exti.h"
#include "stm32l4xx_ll_cortex.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_usart.h"
#include "stm32l4xx_ll_spi.h"

#include "defines.h"
#include "debug.h"

static SPI_TypeDef *SPI = SPI2;

uint8_t data[1024];
uint32_t i = 0;

void SPI_Test(void);

/**
 * @brief Initializes the SPI
 */
int SPI_Init(void)
{
	if (LL_SPI_IsEnabled(SPI))
		return ALREADY_IN_USE_ERROR;

	LL_SPI_InitTypeDef SPI_Init = {
			.TransferDirection = LL_SPI_FULL_DUPLEX,
			.Mode = LL_SPI_MODE_SLAVE,
			.DataWidth = LL_SPI_DATAWIDTH_8BIT,
			.ClockPolarity = LL_SPI_POLARITY_LOW,
			.ClockPhase = LL_SPI_PHASE_1EDGE,
			.NSS = LL_SPI_NSS_HARD_INPUT,
			.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV2,
			.BitOrder = LL_SPI_MSB_FIRST,
			.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE
	};

	LL_GPIO_InitTypeDef GPIO_Init = {
			.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3,
			.Mode = LL_GPIO_MODE_ALTERNATE,
			.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
			.OutputType = LL_GPIO_OUTPUT_PUSHPULL,
			.Pull = LL_GPIO_PULL_NO,
			.Alternate = LL_GPIO_AF_5
	};

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);

	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC);
	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOB);

	LL_GPIO_Init(GPIOC, &GPIO_Init);

	GPIO_Init.Pin = LL_GPIO_PIN_10 | LL_GPIO_PIN_12;
	LL_GPIO_Init(GPIOB, &GPIO_Init);

	LL_SPI_Init(SPI, &SPI_Init);
	LL_SPI_SetStandard(SPI, LL_SPI_PROTOCOL_MOTOROLA);
	LL_SPI_DisableNSSPulseMgt(SPI);

	LL_SPI_SetRxFIFOThreshold(SPI, LL_SPI_RX_FIFO_TH_QUARTER);

	LL_SPI_EnableIT_ERR(SPI);

	NVIC_SetPriority(SPI2_IRQn, 10);
	NVIC_EnableIRQ(SPI2_IRQn);

	LL_GPIO_InitTypeDef GPIO_2 = {
			.Pin = LL_GPIO_PIN_13,
			.Mode = LL_GPIO_MODE_INPUT,
			.Speed = LL_GPIO_SPEED_FREQ_LOW,
			.Pull = LL_GPIO_PULL_NO
	};

	LL_GPIO_Init(GPIOC, &GPIO_2);

	LL_EXTI_InitTypeDef exti_def = {
		.Line_0_31 = LL_EXTI_LINE_13,
		.LineCommand = ENABLE,
		.Mode = LL_EXTI_MODE_IT,
		.Trigger = LL_EXTI_TRIGGER_RISING
	};
	LL_EXTI_Init(&exti_def);
	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTC, LL_SYSCFG_EXTI_LINE13);

	NVIC_SetPriority(EXTI15_10_IRQn, 15);
	NVIC_EnableIRQ(EXTI15_10_IRQn);

	SPI_Test();

	return OK;
}

int SPI_Send(uint8_t *tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len)
{
	if (LL_SPI_IsEnabled(SPI))
		return ALREADY_IN_USE_ERROR;

	return OK;
}

void SPI_Test(void)
{
	LL_SPI_Enable(SPI);
	i = 11;
	uint8_t d;

	while (LL_SPI_IsActiveFlag_RXNE(SPI))
		d = LL_SPI_ReceiveData8(SPI);

	while (!LL_SPI_IsActiveFlag_TXE(SPI));
	LL_SPI_TransmitData8(SPI, 0);

	for (;;) {
		while (!LL_SPI_IsActiveFlag_RXNE(SPI));
		d = LL_SPI_ReceiveData8(SPI);

		if (d != 0) {
			switch (d) {
			case 0x42:
				d = 0x13;
				break;
			default:
				d = ++i;
			}
		} else {
			d = 0;
		}

		while (!LL_SPI_IsActiveFlag_TXE(SPI));
		LL_SPI_TransmitData8(SPI, d);
	}
}

void SPI2_IRQHandler(void)
{
	debug("Error occurred\n\r");
	if (LL_SPI_IsActiveFlag_OVR(SPI)) {
		debug("OVERRUN err\n\r");
		LL_SPI_ClearFlag_OVR(SPI);
	}
}

void EXTI15_10_IRQHandler(void)
{
	LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_13);
	data[i] = '\0';
	i = 0;
	debug("Received frame %s\n\r", data);
}
