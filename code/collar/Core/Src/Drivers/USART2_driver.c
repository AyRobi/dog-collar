/*
 * USART2_driver.c
 *
 *  Author: AyRobi
 */

#include "Drivers/USART2_driver.h"

#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_exti.h"
#include "stm32l4xx_ll_cortex.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_usart.h"

#include "defines.h"

/**
 * @brief Initializes USART2 with the following parameters
 * 		+ baud rate : DEBUG_BAUD_RATE bauds
 * 		+ data width : 8 bits
 * 		+ stop bits : 1
 * 		+ parity : none
 * 	@return int
 */
int USART2_Init(void)
{
	if (LL_USART_IsEnabled(USART2))
		return ALREADY_IN_USE_ERROR;

	LL_USART_InitTypeDef u_def = {
			.BaudRate = DEBUG_BAUD_RATE,
			.DataWidth = LL_USART_DATAWIDTH_8B,
			.Parity = LL_USART_PARITY_NONE,
			.StopBits = LL_USART_STOPBITS_1,
			.TransferDirection = LL_USART_DIRECTION_TX_RX,
			.HardwareFlowControl = LL_USART_HWCONTROL_NONE,
			.OverSampling = LL_USART_OVERSAMPLING_16

	};

	LL_GPIO_InitTypeDef gpio_def = {
			.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3,
			.Mode = LL_GPIO_MODE_ALTERNATE,
			.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
			.OutputType = LL_GPIO_OUTPUT_PUSHPULL,
			.Pull = LL_GPIO_PULL_NO,
			.Alternate = LL_GPIO_AF_7
	};


	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);
	LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);

	LL_GPIO_Init(GPIOA, &gpio_def);

	LL_USART_Init(USART2, &u_def);
	LL_USART_ConfigAsyncMode(USART2);
	LL_USART_Enable(USART2);

	return OK;
}

/**
 * @brief 	Transmits 8 bits through the USART2 tx line
 * 			Warning : this is the user task to insure that the USART is initialized
 *
 * @param data : the 8 bits to be transferred
 *
 * @return int
 */
int USART2_TransmitData(uint8_t data)
{
	if (!LL_USART_IsActiveFlag_TXE(USART2))
		return BUSY_ERROR;

	LL_USART_TransmitData8(USART2, data);

	return OK;
}

/**
 * @brief 	Transmits data through the USART2 tx line
 * 			Warning : this is the user task to insure that the USART is initialized
 *
 * @param data : the 8 bits data array to be transferred
 * @param len : number of data to transmit
 *
 * @return int
 */
void USART2_Transmit(uint8_t *data, uint32_t len)
{

	for (uint32_t i = 0; i < len; ++i) {
		while (!LL_USART_IsActiveFlag_TXE(USART2));

		LL_USART_TransmitData8(USART2, data[i]);
	}
}
