/*
 * neo6m.c
 *
 *  Author: AyRobi
 */

#include "Drivers/LPUART1_driver.h"
#include "debug.h"

#include "neo6m.h"

int NEO_6M_Init(void)
{
	return LPUART1_Init();
}
