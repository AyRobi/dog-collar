/*
 * debug.c
 *
 *  Author: AyRobi
 */


#include "debug.h"

#ifdef DEBUG_LOG
static char buff[DEBUG_BUFF_SIZE];
#endif

void debug_Init(void)
{
#ifdef DEBUG_LOG
	USART2_Init();
#endif
}

void debug(char *data, ...)
{
#ifdef DEBUG_LOG
	va_list args;
	va_start(args, data);
	uint32_t l = vsnprintf(buff, DEBUG_BUFF_SIZE, data, args);
	USART2_Transmit((uint8_t*) buff, l);
#endif
}
