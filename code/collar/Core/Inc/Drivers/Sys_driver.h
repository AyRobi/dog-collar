/*
 * Sys_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_SYS_DRIVER_H_
#define INC_DRIVERS_SYS_DRIVER_H_

int Sys_Driver_Init(void);

#endif /* INC_DRIVERS_SYS_DRIVER_H_ */
