/*
 * GPIO_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_GPIO_DRIVER_H_
#define INC_DRIVERS_GPIO_DRIVER_H_

int GPIO_Driver_Init(void);

#endif /* INC_DRIVERS_GPIO_DRIVER_H_ */
