/*
 * USART2_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_USART2_DRIVER_H_
#define INC_DRIVERS_USART2_DRIVER_H_

#include <stdint.h>

int USART2_Init(void);

int USART2_TransmitData(uint8_t data);

void USART2_Transmit(uint8_t *data, uint32_t len);

#endif /* INC_DRIVERS_USART2_DRIVER_H_ */
