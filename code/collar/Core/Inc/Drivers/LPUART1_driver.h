/*
 * LPUART1_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_LPUART1_DRIVER_H_
#define INC_DRIVERS_LPUART1_DRIVER_H_

#include <stdint.h>

int LPUART1_Init(void);
int LPUART1_Send(uint8_t *data, uint32_t len);

#endif /* INC_DRIVERS_LPUART1_DRIVER_H_ */
