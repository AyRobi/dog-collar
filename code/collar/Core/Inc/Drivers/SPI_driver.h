/*
 * SPI_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_SPI_DRIVER_H_
#define INC_DRIVERS_SPI_DRIVER_H_

#include <stdint.h>

int SPI_Init(void);

int SPI_Send(uint8_t *tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len);
void SPI_Test(void);

#endif /* INC_DRIVERS_SPI_DRIVER_H_ */
