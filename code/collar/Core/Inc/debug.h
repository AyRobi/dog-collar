/*
 * debug.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#include "defines.h"

#ifdef DEBUG_LOG
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "Drivers/USART2_driver.h"
#endif

void debug_Init(void);

void debug(char *data, ...);

#endif /* INC_DEBUG_H_ */
