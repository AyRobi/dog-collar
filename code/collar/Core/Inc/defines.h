/*
 * defines.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DEFINES_H_
#define INC_DEFINES_H_

/* ---Common includes--- */
#include "config.h"

/* --Enumerations-- */

/* ----Return code definitions---- */
#define OK 0
#define UNKNOWN_ERROR -1
#define ALREADY_IN_USE_ERROR -2
#define BUSY_ERROR -3
#define OS_INITIALIZATION_ERROR -4
#define NOT_INITIALIZED_ERROR -5


#endif /* INC_DEFINES_H_ */
