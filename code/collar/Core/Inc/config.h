/*
 * config.h
 *
 *  Author: AyRobi
 */

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

#define DEBUG_BAUD_RATE 115200
#define DEBUG_BUFF_SIZE 528
#define DEBUG_LOG

#endif /* INC_CONFIG_H_ */
