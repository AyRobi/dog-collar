/*
 * system.h
 *
 *  Author: AyRobi
 */

#ifndef INC_SYSTEM_H_
#define INC_SYSTEM_H_

#include "cmsis_gcc.h"

#include "Drivers/Sys_driver.h"

__STATIC_INLINE int System_Init(void)
{
	return Sys_Driver_Init();
}

#endif /* INC_SYSTEM_H_ */
