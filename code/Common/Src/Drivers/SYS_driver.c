/*
 * Sys_driver.c
 *
 *  Author: AyRobi
 */

#include "defines.h"

#include "Drivers/ll_inc.h"

#include "Drivers/Sys_driver.h"

int Sys_Driver_Init(void)
{
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(0);

	Sys_Driver_Clock_Config();

	return OK;
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void Sys_Driver_Clock_Config(void)
{
	LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE2);

	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	while (LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_2);

	LL_FLASH_EnableInstCache();
	LL_FLASH_EnablePrefetch();
	LL_FLASH_EnableDataCache();

	LL_RCC_HSE_EnableBypass();
	LL_RCC_HSE_Enable();

	/* Wait till HSE is ready */
	while (!LL_RCC_HSE_IsReady());

	LL_RCC_PLL_Disable();

	LL_RCC_PLL_ConfigDomain_48M(
			LL_RCC_PLLSOURCE_HSE,
			LL_RCC_PLLM_DIV_8,
			336,
			LL_RCC_PLLQ_DIV_7
	);

	LL_RCC_PLL_ConfigDomain_SYS(
			LL_RCC_PLLSOURCE_HSE,
			LL_RCC_PLLM_DIV_8,
			336,
			LL_RCC_PLLP_DIV_4
	);

	LL_RCC_PLL_Enable();

	while (!LL_RCC_PLL_IsReady());

	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

	/* Wait till System clock is ready */
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
		LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

	LL_RCC_HSI_Disable();

	SystemCoreClockUpdate();
	LL_RCC_SetTIMPrescaler(LL_RCC_TIM_PRESCALER_TWICE);
}
