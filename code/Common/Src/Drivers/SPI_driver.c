/*
 * SPI_driver.c
 *
 *  Author: AyRobi
 */

#include "Drivers/SPI_driver.h"

#include "Drivers/ll_inc.h"
#include "defines.h"
#include "debug.h"

#define SPI_TX_DMA_CHANNEL 				LL_DMA_CHANNEL_0
#define SPI_TX_DMA_STREAM 				LL_DMA_STREAM_4
#define SPI_TX_DMA_IRQ 					DMA1_Stream4_IRQn

#define SPI_RX_DMA_CHANNEL 				LL_DMA_CHANNEL_0
#define SPI_RX_DMA_STREAM 				LL_DMA_STREAM_3
#define SPI_RX_DMA_IRQ 					DMA1_Stream3_IRQn

#define SPI								SPI2
#define DMA								DMA1

__WEAK void SPI_On_TC(void);

__STATIC_INLINE void SPI_DMA_TX_Clear(void)
{
	LL_DMA_DisableStream(DMA, SPI_TX_DMA_STREAM);
	while (LL_DMA_IsEnabledStream(DMA, SPI_TX_DMA_STREAM));

	WRITE_REG(
			DMA->HIFCR,
			DMA_HIFCR_CTCIF4 | DMA_HIFCR_CHTIF4 |
			DMA_HIFCR_CTEIF4 | DMA_HIFCR_CDMEIF4 |
			DMA_HIFCR_CFEIF4);
}

__STATIC_INLINE void SPI_DMA_RX_Clear(void)
{
	LL_DMA_DisableStream(DMA, SPI_RX_DMA_STREAM);
	while (LL_DMA_IsEnabledStream(DMA, SPI_RX_DMA_STREAM));

	WRITE_REG(
			DMA->LIFCR,
			DMA_LIFCR_CTCIF3 | DMA_LIFCR_CHTIF3 |
			DMA_LIFCR_CTEIF3 | DMA_LIFCR_CDMEIF3 |
			DMA_LIFCR_CFEIF3);
}

__STATIC_INLINE uint32_t SPI_DMA_TX_IsComplete(void)
{
	return LL_DMA_IsActiveFlag_TC4(DMA);
}

__STATIC_INLINE uint32_t SPI_DMA_RX_IsComplete(void)
{
	return LL_DMA_IsActiveFlag_TC3(DMA);
}

__STATIC_INLINE int SPI_IsTC(void)
{
	return !LL_DMA_GetDataLength(DMA, SPI_TX_DMA_STREAM) &&
			!LL_DMA_GetDataLength(DMA, SPI_RX_DMA_STREAM);
}

__STATIC_INLINE int SPI_Transfert_Init(uint8_t const* tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len)
{
	if (LL_SPI_IsEnabled(SPI) || LL_SPI_IsActiveFlag_BSY(SPI) ||
			LL_DMA_IsEnabledStream(DMA, SPI_TX_DMA_STREAM) ||
			LL_DMA_IsEnabledStream(DMA, SPI_RX_DMA_STREAM))
		return BUSY_ERROR;

	LL_DMA_InitTypeDef DMA_Init = {
			.Channel = SPI_TX_DMA_CHANNEL,
			.Direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
			.FIFOMode = LL_DMA_FIFOMODE_ENABLE,
			.FIFOThreshold = LL_DMA_FIFOTHRESHOLD_1_2,
			.MemBurst = LL_DMA_MBURST_INC8,
			.PeriphBurst = LL_DMA_PBURST_SINGLE,
			.Priority = LL_DMA_PRIORITY_VERYHIGH,
			.PeriphOrM2MSrcAddress = (uint32_t) &(SPI->DR),
			.PeriphOrM2MSrcDataSize = LL_DMA_MDATAALIGN_BYTE,
			.PeriphOrM2MSrcIncMode = LL_DMA_MEMORY_NOINCREMENT,
			.Mode = LL_DMA_MODE_NORMAL,
			.MemoryOrM2MDstDataSize = LL_DMA_PDATAALIGN_BYTE,
			.MemoryOrM2MDstIncMode = LL_DMA_MEMORY_INCREMENT,
			.MemoryOrM2MDstAddress = (uint32_t) &tx[0],
			.NbData = tx_len
	};

	NVIC_DisableIRQ(SPI_TX_DMA_IRQ);
	NVIC_DisableIRQ(SPI_RX_DMA_IRQ);

	SPI_DMA_TX_Clear();
	SPI_DMA_RX_Clear();

	LL_SPI_DisableDMAReq_TX(SPI);
	LL_SPI_DisableDMAReq_RX(SPI);

	LL_DMA_Init(DMA, SPI_TX_DMA_STREAM, &DMA_Init);

	DMA_Init.Direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY;
	DMA_Init.MemoryOrM2MDstAddress = (uint32_t) &rx[0];
	DMA_Init.NbData = rx_len;
	LL_DMA_Init(DMA, SPI_RX_DMA_STREAM, &DMA_Init);

	__DSB();

	LL_DMA_EnableStream(DMA, SPI_TX_DMA_STREAM);
	LL_DMA_EnableStream(DMA, SPI_RX_DMA_STREAM);

	return OK;
}

__STATIC_INLINE void SPI_Start(void)
{
	LL_SPI_Enable(SPI);

	LL_SPI_EnableDMAReq_TX(SPI);
	LL_SPI_EnableDMAReq_RX(SPI);
}

__STATIC_INLINE void SPI_Close(void)
{
	LL_DMA_DisableStream(DMA, SPI_TX_DMA_STREAM);
	LL_DMA_DisableStream(DMA, SPI_RX_DMA_STREAM);

	while (LL_DMA_IsEnabledStream(DMA, SPI_TX_DMA_STREAM));
	while (LL_DMA_IsEnabledStream(DMA, SPI_RX_DMA_STREAM));

	while (!LL_SPI_IsActiveFlag_TXE(SPI));
	while (LL_SPI_IsActiveFlag_BSY(SPI));

	LL_SPI_Disable(SPI);

	__DSB();
}

/**
 * @brief Initializes the SPI
 */
int SPI_Init(void)
{
	if (LL_SPI_IsEnabled(SPI))
		return ALREADY_IN_USE_ERROR;

	LL_SPI_InitTypeDef SPI_Init = {
			.TransferDirection = LL_SPI_FULL_DUPLEX,
			.Mode = LL_SPI_MODE_MASTER,
			.DataWidth = LL_SPI_DATAWIDTH_8BIT,
			.ClockPolarity = LL_SPI_POLARITY_LOW,
			.ClockPhase = LL_SPI_PHASE_1EDGE,
			.NSS = LL_SPI_NSS_HARD_OUTPUT,
			.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV256,
			.BitOrder = LL_SPI_MSB_FIRST,
			.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE
	};

	LL_GPIO_InitTypeDef GPIO_Init = {
			.Pin = LL_GPIO_PIN_2 | LL_GPIO_PIN_3,
			.Mode = LL_GPIO_MODE_ALTERNATE,
			.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
			.OutputType = LL_GPIO_OUTPUT_PUSHPULL,
			.Pull = LL_GPIO_PULL_NO,
			.Alternate = LL_GPIO_AF_5
	};

	/* Peripheral clock enable */
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

	LL_GPIO_Init(GPIOC, &GPIO_Init);

	GPIO_Init.Pin = LL_GPIO_PIN_10 | LL_GPIO_PIN_12;
	LL_GPIO_Init(GPIOB, &GPIO_Init);

	LL_SPI_Init(SPI, &SPI_Init);
	LL_SPI_SetStandard(SPI, LL_SPI_PROTOCOL_MOTOROLA);

	LL_DMA_EnableIT_TC(DMA, SPI_TX_DMA_STREAM);
	LL_DMA_EnableIT_TC(DMA, SPI_RX_DMA_STREAM);

	NVIC_SetPriority(SPI_TX_DMA_IRQ, 10);
	NVIC_SetPriority(SPI_RX_DMA_IRQ, 10);

	return OK;
}

int SPI_BeginTransfert(uint8_t const* tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len)
{
	int res = SPI_Transfert_Init(tx, tx_len, rx, rx_len);
	if (res != OK)
		return res;

	SPI_Start();

	NVIC_EnableIRQ(SPI_TX_DMA_IRQ);
	NVIC_EnableIRQ(SPI_RX_DMA_IRQ);

	return OK;
}

int SPI_EndTransfert(void)
{
	if (!SPI_IsTC())
		return BUSY_ERROR;

	SPI_Close();

	return OK;
}

int SPI_Send_Sync(uint8_t const* tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len)
{
	int res = SPI_Transfert_Init(tx, tx_len, rx, rx_len);
	if (res != OK)
		return res;

	SPI_Start();

	while ((!SPI_DMA_TX_IsComplete() && tx_len) ||
			(!SPI_DMA_RX_IsComplete() && rx_len));

	SPI_Close();

	return OK;
}

__STATIC_INLINE void SPI_Check_TC(void)
{
	if (SPI_IsTC())
		SPI_On_TC();
}

void DMA1_Stream3_IRQHandler(void)
{
	LL_DMA_ClearFlag_TC3(DMA);

	SPI_Check_TC();
}

void DMA1_Stream4_IRQHandler(void)
{
	LL_DMA_ClearFlag_TC4(DMA);

	SPI_Check_TC();
}
