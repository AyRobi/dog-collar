/*
 * PWR_driver.c
 *
 *  Author: AyRobi
 */

#include "Drivers/ll_inc.h"

#include "Drivers/PWR_driver.h"
#include "Drivers/Sys_driver.h"

void PWR_EnterSleepMode(void)
{
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
	LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_MAINREGU);

	LL_LPM_EnableSleep();

	__WFI();
}

void PWR_EnterStopMode(void)
{
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
	LL_PWR_SetPowerMode(LL_PWR_MODE_STOP_LPREGU_DEEPSLEEP);

	__disable_irq();

	LL_LPM_EnableDeepSleep();
	LL_SYSTICK_DisableIT();

	__WFI();

	Sys_Driver_Clock_Config();
	LL_SYSTICK_EnableIT();

	__enable_irq();
}
