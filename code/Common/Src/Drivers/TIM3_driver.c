/*
 * TIM3_driver.c
 *
 *  Author: AyRobi
 */
#include "Drivers/TIM3_driver.h"

#include "Drivers/ll_inc.h"

#include "debug.h"
#include "defines.h"

#define TIM 			TIM3
#define TIM_Irq 		TIM3_IRQn

__STATIC_INLINE void __EnableTimClock(void)
{
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
}

__STATIC_INLINE void __DisableTimClock(void)
{
	LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_TIM3);
}

int TIM3_StartMs(uint16_t ms)
{
	if (ms == 0)
		return OK;

	if (LL_TIM_IsEnabledCounter(TIM))
		return BUSY_ERROR;

	LL_TIM_InitTypeDef TIM_init = {
			.Prescaler = SystemCoreClock / 2000 - 1,
			.CounterMode = LL_TIM_COUNTERMODE_DOWN,
			.Autoreload = 2 * ms - 1,
			.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1
	};

	NVIC_DisableIRQ(TIM_Irq);
	__EnableTimClock();

	LL_TIM_Init(TIM, &TIM_init);
	LL_TIM_DisableARRPreload(TIM);
	LL_TIM_SetClockSource(TIM, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetOnePulseMode(TIM, LL_TIM_ONEPULSEMODE_SINGLE);
	LL_TIM_SetTriggerOutput(TIM, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM);
	LL_TIM_EnableIT_UPDATE(TIM);

	LL_TIM_EnableCounter(TIM);

	NVIC_SetPriority(TIM_Irq, 15);
	NVIC_EnableIRQ(TIM_Irq);

	return OK;
}

int TIM3_Restart(void)
{
	if (!LL_TIM_IsEnabledCounter(TIM))
		return NOT_SET_ERROR;

	LL_TIM_GenerateEvent_UPDATE(TIM);
	LL_TIM_EnableCounter(TIM);

	return OK;
}

uint16_t TIM3_RemainingMs()
{
	if (LL_TIM_IsEnabledCounter(TIM))
		return LL_TIM_GetCounter(TIM) / 2 + 1;
	else
		return 0;
}

void TIM3_IRQHandler(void)
{
	LL_TIM_ClearFlag_UPDATE(TIM);

	if (!LL_TIM_IsEnabledCounter(TIM))
		__DisableTimClock();
}
