/*
 * sx1276_gpio_driver.c
 *
 *  Author: AyRobi
 */

#include "Drivers/sx1276_gpio_driver.h"

#include "Drivers/ll_inc.h"
#include "defines.h"

#define RESET_PIN		LL_GPIO_PIN_4
#define RESET_GPIO		GPIOB

__STATIC_INLINE void __EnableClocks(void)
{
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
}

int SX1276_GPIO_Drivers_Init(void)
{
	__EnableClocks();

	SX1276_GPIO_Drivers_ClearReset();

	return OK;
}

void SX1276_GPIO_Drivers_SetReset(void)
{
	LL_GPIO_InitTypeDef reset_def = {
			.Pin = RESET_PIN,
			.Mode = LL_GPIO_MODE_OUTPUT,
			.Speed = LL_GPIO_SPEED_FREQ_LOW,
			.OutputType = LL_GPIO_OUTPUT_PUSHPULL
	};

	LL_GPIO_Init(RESET_GPIO, &reset_def);

	LL_GPIO_ResetOutputPin(RESET_GPIO, RESET_PIN);
}

void SX1276_GPIO_Drivers_ClearReset(void)
{
	LL_GPIO_InitTypeDef reset_def = {
			.Pin = RESET_PIN,
			.Mode = LL_GPIO_MODE_INPUT,
			.Speed = LL_GPIO_SPEED_FREQ_LOW,
			.Pull = LL_GPIO_PULL_NO
	};

	LL_GPIO_Init(RESET_GPIO, &reset_def);
}
