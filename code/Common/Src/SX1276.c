/*
 * sx1276.c
 *
 *  Author: AyRobi
 */

#include "sx1276.h"

#include <stddef.h>
#include <cmsis_gcc.h>
#include <cmsis_os.h>
#include "Drivers/SPI_driver.h"
#include "Drivers/sx1276_gpio_driver.h"
#include "Drivers/TIM3_driver.h"
#include "debug.h"
#include "os_config.h"
#include "defines.h"

#define REG_FIFO 						((uint8_t) 0x00)

/* Common Register settings */
#define REG_OP_MODE 					((uint8_t) 0x01)
#define REG_FRF_MSB 					((uint8_t) 0x06)
#define REG_FRF_MID 					((uint8_t) 0x07)
#define REG_FRF_LSB 					((uint8_t) 0x08)

/* Registers for RF blocks */
#define REG_PA_CONFIG 					((uint8_t) 0x09)
#define REG_PA_RAMP 					((uint8_t) 0x0A)
#define REG_OCP 						((uint8_t) 0x0B)
#define REG_LNA 						((uint8_t) 0x0C)

/* Lora page registers */
#define REG_FIFO_ADDR_PTR 				((uint8_t) 0x0D)
#define REG_FIFO_TX_BASE_ADDR 			((uint8_t) 0x0E)
#define REG_FIFO_RX_BASE_ADDR 			((uint8_t) 0x0F)
#define REG_FIFO_RX_CURRENT_ADDR 		((uint8_t) 0x10)
#define REG_IRQ_FLAGS_MASK 				((uint8_t) 0x11)
#define REG_IRQ_FLAGS 					((uint8_t) 0x12)
#define REG_RX_NB_BYTES					((uint8_t) 0x13)
#define REG_RX_HEADER_CNT_VALUE_MSB		((uint8_t) 0x14)
#define REG_RX_HEADER_CNT_VALUE_LSB		((uint8_t) 0x15)
#define REG_RX_PACKET_CNT_VALUE_MSB		((uint8_t) 0x16)
#define REG_RX_PACKET_CNT_VALUE_LSB		((uint8_t) 0x17)
#define REG_RX_MODEM_STAT				((uint8_t) 0x18)
#define REG_PKT_SNR_VALUE				((uint8_t) 0x19)
#define REG_PKT_RSSI_VALUE				((uint8_t) 0x1A)
#define REG_RSSI_VALUE					((uint8_t) 0x1B)
#define REG_HOP_CHANNEL					((uint8_t) 0x1C)
#define REG_MODEM_CONFIG1				((uint8_t) 0x1D)
#define REG_MODEM_CONFIG2				((uint8_t) 0x1E)
#define REG_SYMB_TIMEOUT_LSB			((uint8_t) 0x1F)
#define REG_PREAMBLE_MSB				((uint8_t) 0x20)
#define REG_PREAMBLE_LSB				((uint8_t) 0x21)
#define REG_PAYLOAD_LENGTH				((uint8_t) 0x22)
#define REG_MAX_PAYLOAD_LENGTH			((uint8_t) 0x23)
#define REG_HOP_PERIOD					((uint8_t) 0x24)
#define REG_FIFO_RX_BYTE_ADDR			((uint8_t) 0x25)
#define REG_MODEM_CONFIG_3				((uint8_t) 0x26)
#define REG_FEI_MSB						((uint8_t) 0x28)
#define REG_FEI_MID						((uint8_t) 0x29)
#define REG_FEI_LSB						((uint8_t) 0x2A)
#define REG_RSSI_WIDEBAND				((uint8_t) 0x2C)
#define REG_DETECT_OPTIMIZE				((uint8_t) 0x31)
#define REG_INVERT_IQ					((uint8_t) 0x33)
#define REG_HIGH_BW_OPTIMIZE1			((uint8_t) 0x36)
#define REG_DETECTION_THRESHOLD			((uint8_t) 0x37)
#define REG_SYNC_WORD					((uint8_t) 0x39)
#define REG_HIGH_BW_OPTIMIZE2			((uint8_t) 0x3A)
#define REG_INVERT_IQ2					((uint8_t) 0x3B)
#define REG_DIO_MAPPING1				((uint8_t) 0x40)
#define REG_DIO_MAPPING2				((uint8_t) 0x41)
#define REG_VERSION						((uint8_t) 0x42)
#define REG_TCXO						((uint8_t) 0x4B)
#define REG_PA_DAC						((uint8_t) 0x4C)
#define REG_FORMER_TEMP					((uint8_t) 0x4D)
#define REG_AGC_REF						((uint8_t) 0x61)
#define REG_AGC_THRESH1					((uint8_t) 0x62)
#define REG_AGC_THRESH2					((uint8_t) 0x63)
#define REG_AGC_THRESH3					((uint8_t) 0x64)
#define REG_PLL							((uint8_t) 0x70)

#define OP_MODE_MSK						((uint8_t) 0x7)
#define PA_RAMP_MSK						((uint8_t) 0xf)

#define TO_READ_OP(addr)				(addr)
#define TO_WRITE_OP(addr)				(addr | (1 << 7))

#define RESET_LOW_MS 1
#define RESET_HIGH_MS 6

__STATIC_INLINE void WRITE_SINGLE_SYNC(uint8_t addr, uint8_t val);
__STATIC_INLINE uint8_t READ_SINGLE_SYNC(uint8_t addr);
__STATIC_INLINE void WRITE_SINGLE(uint8_t addr, uint8_t val);
__STATIC_INLINE uint8_t READ_SINGLE(uint8_t addr);

__STATIC_INLINE void SX1276_Reset(void);
__STATIC_INLINE uint8_t SX1276_GetVersion(void);
__STATIC_INLINE void SX1276_SetOpMode(SX1276_Mode mode);

static StaticSemaphore_t sx1276_sem_cb;
static osSemaphoreId_t sx1276_sem;

const osSemaphoreAttr_t sx1276_sem_attr = {
		.name = "SX1276_semaphore",
		.attr_bits = 0,
		.cb_mem = &sx1276_sem_cb,
		.cb_size = sizeof(StaticSemaphore_t)
};

static int is_working;

int SX1276_Init(SX1276_InitStruct const* params)
{
	uint8_t res;
	int err = SPI_Init();

	if (err != OK)
		return err;

	sx1276_sem = osSemaphoreNew(1, 0, &sx1276_sem_attr);
	if (sx1276_sem == NULL)
		return OS_INITIALIZATION_ERROR;

	is_working = 0;

	SX1276_GPIO_Drivers_Init();

	SX1276_Reset();

	res = READ_SINGLE(REG_VERSION);
	if (res != 0x12)
		return INVALID_HARDWARE_ERROR;

	//TODO do sx1276 initialization

	return OK;
}

void SX1276_Send(uint8_t pid, uint8_t *data, uint32_t len)
{
	//TODO
	uint8_t res = READ_SINGLE(REG_VERSION);
	debug("Version : %d\n\r", res);
}

bool SX1276_CanSTOP(void)
{
	return is_working == 0;
}

__STATIC_INLINE uint8_t SX1276_GetVersion(void)
{
	return READ_SINGLE_SYNC(REG_VERSION);
}

__STATIC_INLINE void SX1276_SetOpMode(SX1276_Mode mode)
{
	uint8_t reg = READ_SINGLE_SYNC(REG_OP_MODE);
	WRITE_SINGLE_SYNC(REG_OP_MODE, (reg & ~OP_MODE_MSK) | ((uint8_t) mode));
}

__STATIC_INLINE void SX1276_Reset(void)
{
	if (TIM3_RemainingMs())
		return;

	SX1276_GPIO_Drivers_SetReset();
	TIM3_StartMs(RESET_LOW_MS);
	while (TIM3_RemainingMs());

	SX1276_GPIO_Drivers_ClearReset();
	TIM3_StartMs(RESET_HIGH_MS);
	while (TIM3_RemainingMs());
}

__STATIC_INLINE uint8_t __SINGLE_TRANSACTION_SYNC(uint8_t command, uint8_t val)
{
	uint8_t data[] = {command, val};
	uint8_t res[2];

	SPI_Send_Sync(data, 2, res, 2);

	return res[1];
}

__STATIC_INLINE void WRITE_SINGLE_SYNC(uint8_t addr, uint8_t val)
{
	__SINGLE_TRANSACTION_SYNC(TO_WRITE_OP(addr), val);
}

__STATIC_INLINE uint8_t READ_SINGLE_SYNC(uint8_t addr)
{
	return __SINGLE_TRANSACTION_SYNC(TO_READ_OP(addr), 0x00);
}

__STATIC_INLINE uint8_t __SINGLE_TRANSACTION(uint8_t command, uint8_t val)
{
	uint8_t data[] = {command, val};
	uint8_t res[2];

	SPI_BeginTransfert(data, 2, res, 2);

	is_working = 1;
	osSemaphoreAcquire(sx1276_sem, osWaitForever);

	is_working = 0;
	SPI_EndTransfert();

	return res[1];
}

__STATIC_INLINE void WRITE_SINGLE(uint8_t addr, uint8_t val)
{
	__SINGLE_TRANSACTION(TO_WRITE_OP(addr), val);
}

__STATIC_INLINE uint8_t READ_SINGLE(uint8_t addr)
{
	return __SINGLE_TRANSACTION(TO_READ_OP(addr), 0x00);
}

void SPI_On_TC(void)
{
	osSemaphoreRelease(sx1276_sem);
}
