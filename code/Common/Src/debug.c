/*
 * debug.c
 *
 *  Author: AyRobi
 */


#include "debug.h"

#include "config.h"

#ifdef DEBUG_LOG
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "Drivers/USART2_driver.h"
#endif

#ifdef DEBUG_LOG

#if !defined(DEBUG_BAUD_RATE)
	#error "'DEBUG_BAUD_RATE' must be defined in 'config.h'"
#endif

#if !defined(DEBUG_BUFF_SIZE)
	#error "'DEBUG_BUFF_SIZE' must be defined in 'config.h'"
#endif

static char buff[DEBUG_BUFF_SIZE];
#endif

void debug_Init(void)
{
#ifdef DEBUG_LOG
	USART2_Init(DEBUG_BAUD_RATE);
#endif
}

void debug(char const* data, ...)
{
#ifdef DEBUG_LOG
	va_list args;
	va_start(args, data);
	uint32_t l = vsnprintf(buff, DEBUG_BUFF_SIZE, data, args);
	USART2_Transmit((uint8_t*) buff, l);
#endif
}

void debug_Flush(void)
{
#ifdef DEBUG_LOG
	USART2_Flush();
#endif
}
