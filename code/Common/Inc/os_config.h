/*
 * os_config.h
 *
 *  Author: AyRobi
 */

#ifndef INC_OS_CONFIG_H_
#define INC_OS_CONFIG_H_

#include "cmsis_os.h"

/* cmsis os complementary type definitions */
typedef StaticTask_t TCB_t;
typedef StaticQueue_t MessageQueueCb_t;
typedef StaticSemaphore_t StaticSemaphore_t;

#endif /* INC_OS_CONFIG_H_ */
