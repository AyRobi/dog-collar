/*
 * defines.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DEFINES_H_
#define INC_DEFINES_H_

/* ----Return code definitions---- */
#define OK							0
#define UNKNOWN_ERROR				-1
#define ALREADY_IN_USE_ERROR		-2
#define BUSY_ERROR					-3
#define OS_INITIALIZATION_ERROR		-4
#define NOT_SET_ERROR				-5
#define INVALID_HARDWARE_ERROR		-6


#endif /* INC_DEFINES_H_ */
