/*
 * TIM3_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_TIM3_DRIVER_H_
#define INC_DRIVERS_TIM3_DRIVER_H_

#include <stdint.h>

/**
 * @brief Starts the TIM3 for a defined amount of time. The timer must be inactive,
 * otherwise a BUSY_ERROR will be returned
 *
 * @param
 * 		- ms: the number of milliseconds for which the timer should be active
 *
 * 	@return a status code (OK or error)
 */
int TIM3_StartMs(uint16_t ms);

/**
 * @brief Restarts the timer with the last recorded time from 'TIM3_StartMs'.
 * Note: the timer must be active.
 *
 * @return a status code (OK or error)
 */
int TIM3_Restart(void);

/**
 * @brief Gets the remaining number of milliseconds before the end of the timer
 *
 * @return uin16_t
 */
uint16_t TIM3_RemainingMs();

#endif /* INC_DRIVERS_TIM3_DRIVER_H_ */
