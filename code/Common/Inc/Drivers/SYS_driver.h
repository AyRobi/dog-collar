/*
 * Sys_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_SYS_DRIVER_H_
#define INC_DRIVERS_SYS_DRIVER_H_

/**
 * @brief Inits the SYS driver
 *
 * @return a status code (OK or error)
 */
int Sys_Driver_Init(void);

/**
 * @brief Sets the core clock and peripheral clocks. Also, configures peripherals like FLASH
 * according to clock speed.
 */
void Sys_Driver_Clock_Config(void);

#endif /* INC_DRIVERS_SYS_DRIVER_H_ */
