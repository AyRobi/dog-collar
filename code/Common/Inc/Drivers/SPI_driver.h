/*
 * SPI_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_SPI_DRIVER_H_
#define INC_DRIVERS_SPI_DRIVER_H_

#include <stdint.h>

/**
 * @brief Inits the SPI driver
 *
 * @return a status code (OK or error)
 */
int SPI_Init(void);

/**
 * @brief Begins an asynchronous data transfer. To handle the transfer completion, the user should
 * implement the function 'void SPI_On_TC(void)', the later will be called in an ISR context.
 *
 * @params
 * 		- tx: an uint8_t array containing data to be transferred
 * 		- tx_len: the number of data to transfer
 * 		- rx: an uint8_t array. It will be filled by received data, its size is supposed to be sufficient
 * 			  to handle the whole transfer
 * 		- rx_len: the number of characters to receive, must be lower or equal than 'tx_len'
 *
 * @return a status code (OK or error)
 */
int SPI_BeginTransfert(uint8_t const* tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len);

/**
 * @brief Ends the data transfer.
 * Note: all data must be transfered before calling this function otherwise a
 * BUSY_ERROR will be returned
 *
 * @return a status code (OK or error)
 */
int SPI_EndTransfert(void);

/**
 * @brief Begins a synchronous data transfer.
 * Note: active waiting is used in this function
 *
 * @params
 * 		- tx: an uint8_t array containing data to be transferred
 * 		- tx_len: the number of data to transfer
 * 		- rx: an uint8_t array. It will be filled by received data, its size is supposed to be sufficient
 * 			  to handle the whole transfer
 * 		- rx_len: the number of characters to receive, must be lower or equal than 'tx_len'
 *
 * @return a status code (OK or error)
 */
int SPI_Send_Sync(uint8_t const* tx, uint32_t tx_len, uint8_t *rx, uint32_t rx_len);

#endif /* INC_DRIVERS_SPI_DRIVER_H_ */
