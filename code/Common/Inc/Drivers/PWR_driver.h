/*
 * PWR_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_PWR_DRIVER_H_
#define INC_DRIVERS_PWR_DRIVER_H_

/**
 * @brief Enters in sleep low power mode, Cortex M4 is stopped but peripherals kept running.
 */
void PWR_EnterSleepMode(void);

/**
 * @brief Enters in stop low power mode, Cortex M4 and peripherals are stopped.
 */
void PWR_EnterStopMode(void);

#endif /* INC_DRIVERS_PWR_DRIVER_H_ */
