/*
 * USART2_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_USART2_DRIVER_H_
#define INC_DRIVERS_USART2_DRIVER_H_

#include <stdint.h>

/**
 * @brief Inits the USART2
 *
 * @return a status code (OK or error)
 */
int USART2_Init(uint32_t baud_rate);

/**
 * @brief Transmits a single data through the USART connection
 *
 * @return a status code (OK or error)
 */
int USART2_TransmitData(uint8_t data);

/**
 * @brief Transmits a set of data through the USART2 connection
 *
 * @param
 * 		- data: an array of uint8_t to be transmitted
 * 		- len: the number of data to send
 */
void USART2_Transmit(uint8_t const* data, uint32_t len);

/**
 * @brief Flushes the USART2 TX line
 */
void USART2_Flush(void);

#endif /* INC_DRIVERS_USART2_DRIVER_H_ */
