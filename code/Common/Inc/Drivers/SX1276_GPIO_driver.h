/*
 * sx1276_gpio_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_SX1276_GPIO_DRIVER_H_
#define INC_DRIVERS_SX1276_GPIO_DRIVER_H_

/**
 * @brief Inits the GPIO port to communicate with the SX1276 GPIO port
 *
 * @return a status code (OK or error)
 */
int SX1276_GPIO_Drivers_Init(void);

/**
 * @brief Activates the reset pin
 */
void SX1276_GPIO_Drivers_SetReset(void);

/**
 * @brief Releases the reset pin
 */
void SX1276_GPIO_Drivers_ClearReset(void);

#endif /* INC_DRIVERS_SX1276_GPIO_DRIVER_H_ */
