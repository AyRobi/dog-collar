/*
 * ll_inc.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_LL_INC_H_
#define INC_DRIVERS_LL_INC_H_

#if defined(STM32F401xE)
	#include "stm32f4xx_ll_rcc.h"
	#include "stm32f4xx_ll_bus.h"
	#include "stm32f4xx_ll_system.h"
	#include "stm32f4xx_ll_exti.h"
	#include "stm32f4xx_ll_cortex.h"
	#include "stm32f4xx_ll_utils.h"
	#include "stm32f4xx_ll_pwr.h"
	#include "stm32f4xx_ll_dma.h"
	#include "stm32f4xx_ll_gpio.h"
	#include "stm32f4xx_ll_usart.h"
	#include "stm32f4xx_ll_spi.h"
	#include "stm32f4xx_ll_tim.h"
#else
	#error "Error unsupported MCU"
#endif

#endif /* INC_DRIVERS_LL_INC_H_ */
