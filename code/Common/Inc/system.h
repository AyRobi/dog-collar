/*
 * system.h
 *
 *  Author: AyRobi
 */

#ifndef INC_SYSTEM_H_
#define INC_SYSTEM_H_

#include "cmsis_gcc.h"

#include "debug.h"
#include "Drivers/Sys_driver.h"
#include "Drivers/PWR_driver.h"

/**
 * @brief Inits the system drivers. Should be called before using other system functions.
 *
 * @return an int corresponding to the initialization status (e.g OK or an error code)
 */
__STATIC_INLINE int System_Init(void)
{
	return Sys_Driver_Init();
}

/**
 * @brief Puts the chip in sleep mode, the Cortex M4 is stopped but needed peripheral
 * are kept running.
 */
__STATIC_INLINE void System_Sleep(void)
{
	PWR_EnterSleepMode();
}

/**
 * @brief Enters in deepsleep. Here all clocks are stopped, peripherals and Cortex M4 will be
 * stopped.
 */
__STATIC_INLINE void System_DeepSleep(void)
{
	debug("Enter in stop mode\n\r");
	debug_Flush();

	PWR_EnterStopMode();

	debug("Exit from stop mode\n\r");
}

#endif /* INC_SYSTEM_H_ */
