/*
 * utils.h
 *
 *  Author: AyRobi
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include "cmsis_gcc.h"
#include "cmsis_os.h"

/**
 * @brief Converts ticks to milliseconds
 *
 * @return uint32_t
 */
__STATIC_INLINE uint32_t osKernelTickToMs(uint32_t ticks)
{
	return ( ((uint64_t) ticks) * 1000 ) / osKernelGetTickFreq();
}

/**
 * @brief Gets the number of elapsed milliseconds since the kernel start
 * Note:
 * 		- precision depends on kernel ticks granularity
 * 		- discontinuity will appear when the kernel tick counter overflows
 *
 * @return uint32_t
 */
__STATIC_INLINE uint32_t osKernelGetMs(void)
{
	/* Note: precision depends on kernel ticks granularity */
	return osKernelTickToMs(osKernelGetTickCount());
}

__STATIC_INLINE uint32_t osKernelMaxMs(void)
{
	return osKernelTickToMs((uint32_t) -1);
}

#endif /* SRC_UTILS_H_ */
