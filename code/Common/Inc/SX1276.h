/*
 * sx1276.h
 *
 *  Author: AyRobi
 */

#ifndef INC_SX1276_H_
#define INC_SX1276_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	SX1276_MODE_SLEEP			= 0x0,
	SX1276_MODE_STDBY			= 0x1,
	SX1276_MODE_FSTX			= 0x2,
	SX1276_MODE_TX				= 0x3,
	SX1276_MODE_FSRX			= 0x4,
	SX1276_MODE_RX_CONTINUOUS	= 0x5,
	SX1276_MODE_RX_SINGLE		= 0x6,
	SX1276_MODE_CAD				= 0x7
} SX1276_Mode;

typedef enum {
	SX1276_PARAMP_3_4MS			= 0x0,
	SX1276_PARAMP_2MS			= 0x1,
	SX1276_PARAMP_1MS			= 0x2,
	SX1276_PARAMP_500US			= 0x3,
	SX1276_PARAMP_250US			= 0x4,
	SX1276_PARAMP_125US			= 0x5,
	SX1276_PARAMP_100US			= 0x6,
	SX1276_PARAMP_62US			= 0x7,
	SX1276_PARAMP_40US			= 0x8,
	SX1276_PARAMP_31US			= 0x9,
	SX1276_PARAMP_25US			= 0xA,
	SX1276_PARAMP_20US			= 0xB,
	SX1276_PARAMP_15US			= 0xC,
	SX1276_PARAMP_12US			= 0xD,
	SX1276_PARAMP_10US			= 0xE
} SX1276_PaRamp;

typedef enum {
	SX1276_LNA_G1				= 0x1,
	SX1276_LNA_G2				= 0x2,
	SX1276_LNA_G3				= 0x3,
	SX1276_LNA_G4				= 0x4,
	SX1276_LNA_G5				= 0x5,
	SX1276_LNA_G6				= 0x6,
} SX1276_LnaGain;

typedef enum {
	SX1276_BW_7_8KHZ			= 0x0,
	SX1276_BW_10_4KHZ			= 0x1,
	SX1276_BW_15_6KHZ			= 0x2,
	SX1276_BW_20_8KHZ			= 0x3,
	SX1276_BW_31_25KHZ			= 0x4,
	SX1276_BW_41_7KHZ			= 0x5,
	SX1276_BW_62_5KHZ			= 0x6,
	SX1276_BW_125KHZ			= 0x7,
	SX1276_BW_250KHZ			= 0x8,
	SX1276_BW_500KHZ			= 0x9,
} SX1276_Bandwidth;

typedef enum {
	SX1276_CR_5					= 0x1,
	SX1276_CR_6					= 0x2,
	SX1276_CR_7					= 0x3,
	SX1276_CR_8					= 0x4,
} SX1276_CodingRate;

typedef enum {
	SX1276_HEADER_MODE_EXPLICIT	= 0x0,
	SX1276_HEADER_MODE_IMPLICIT	= 0x1
} SX1276_HeaderMode;

typedef enum {
	SX1276_SF_6					= 0x6,
	SX1276_SF_7					= 0x7,
	SX1276_SF_8					= 0x8,
	SX1276_SF_9					= 0x9,
	SX1276_SF_10				= 0xA,
	SX1276_SF_11				= 0xB,
	SX1276_SF_12				= 0xC,
} SX1276_SpreadingFactor;

typedef enum {
	SX1276_CRC_OFF				= 0x0,
	SX1276_CRC_ON				= 0x1,
} SX1276_CRC;

typedef struct {
	uint32_t frequency;
	uint8_t spreadingFactor;
	uint8_t codingRate;
	uint8_t bandwidth;
	uint8_t syncWord;
	bool invertIQ;
} SX1276_InitStruct;

int SX1276_Init(SX1276_InitStruct const* params);
void SX1276_Send(uint8_t pid, uint8_t *data, uint32_t len);
bool SX1276_CanSTOP(void);

#endif /* INC_SX1276_H_ */
