/*
 * debug.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

/**
 * @brief Inits debug logging tool
 */
void debug_Init(void);

/**
 * @brief Logs a string to the debug stream, parameters follows the same rules as defined in standard
 * C function like printf
 *
 * @params
 * 		- data: char array, contains the string to log. Must be null terminated
 * 		- ...: contains optional parameters used for string formatting
 */
void debug(char const* data, ...);

/**
 * @brief Flushes the debug stream.
 */
void debug_Flush(void);

#endif /* INC_DEBUG_H_ */
