/*
 * GPIO_driver.c
 *
 *  Author: AyRobi
 */

#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_usart.h"

#include "defines.h"
#include "debug.h"

#include "Drivers/GPIO_driver.h"

#define LED1_PIN LL_GPIO_PIN_5
#define LED2_PIN LL_GPIO_PIN_6

__WEAK void On_First_UserButton_Click(void);
__WEAK void On_Second_UserButton_Click(void);

__WEAK void On_First_UserButton_Release(void);
__WEAK void On_Second_UserButton_Release(void);

/**
 * @brief Initializes PC0 and PC1 to receive respectively bip command input and locate command input
 *
 * @return int
 */
int GPIO_Driver_Init(void)
{
	LL_GPIO_InitTypeDef btn_def = {
			.Pin = LL_GPIO_PIN_0 | LL_GPIO_PIN_1,
			.Mode = LL_GPIO_MODE_INPUT,
			.Speed = LL_GPIO_SPEED_FREQ_LOW,
			.Pull = LL_GPIO_PULL_UP
	};

	LL_GPIO_InitTypeDef led_def = {
			.Pin = LED1_PIN | LED2_PIN,
			.Mode = LL_GPIO_MODE_OUTPUT,
			.Speed = LL_GPIO_SPEED_FREQ_LOW,
			.OutputType = LL_GPIO_OUTPUT_OPENDRAIN
	};

	LL_EXTI_InitTypeDef exti_def = {
		.Line_0_31 = LL_EXTI_LINE_0,
		.LineCommand = ENABLE,
		.Mode = LL_EXTI_MODE_IT,
		.Trigger = LL_EXTI_TRIGGER_RISING_FALLING
	};


	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);

	LL_GPIO_Init(GPIOC, &btn_def);
	LL_GPIO_Init(GPIOC, &led_def);

	LL_EXTI_Init(&exti_def);
	exti_def.Line_0_31 = LL_EXTI_LINE_1;
	LL_EXTI_Init(&exti_def);

	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTC, LL_SYSCFG_EXTI_LINE0);
	LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTC, LL_SYSCFG_EXTI_LINE1);

	NVIC_SetPriority(EXTI0_IRQn, 10);
	NVIC_SetPriority(EXTI1_IRQn, 10);

	NVIC_EnableIRQ(EXTI0_IRQn);
	NVIC_EnableIRQ(EXTI1_IRQn);

	LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_SYSCFG);

	LL_GPIO_SetOutputPin(GPIOC, LED1_PIN | LED2_PIN);

	return OK;
}

void GPIO_Set_LED1(void)
{
	LL_GPIO_ResetOutputPin(GPIOC, LED1_PIN);
}

void GPIO_Set_LED2(void)
{
	LL_GPIO_ResetOutputPin(GPIOC, LED2_PIN);
}

void GPIO_Reset_LED1(void)
{
	LL_GPIO_SetOutputPin(GPIOC, LED1_PIN);
}

void GPIO_Reset_LED2(void)
{
	LL_GPIO_SetOutputPin(GPIOC, LED2_PIN);
}

void EXTI0_IRQHandler(void)
{
	if (!LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_0))
		return;

	LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);

	if (LL_GPIO_IsInputPinSet(GPIOC, LL_GPIO_PIN_0))
		On_First_UserButton_Release();
	else
		On_First_UserButton_Click();
}

void EXTI1_IRQHandler(void)
{
	if (!LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_1))
		return;

	LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_1);

	if (LL_GPIO_IsInputPinSet(GPIOC, LL_GPIO_PIN_1))
		On_Second_UserButton_Release();
	else
		On_Second_UserButton_Click();
}
