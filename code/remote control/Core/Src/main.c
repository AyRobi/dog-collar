/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
#include "cmsis_os.h"
#include "os_config.h"
#include "system.h"
#include "GPIO.h"
#include "sx1276.h"
#include "debug.h"
#include "config.h"
#include "main_tsk.h"
#include "RF_tsk.h"
#include "LPM_tsk.h"

#define MAIN_TSK_SIZE_64 1024	/* In words of 64-bits */
#define RF_TSK_SIZE_64 1024		/* In words of 64-bits */
#define LPM_TSK_SIZE_64 128		/* In words of 64-bits */

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint64_t main_tsk_stack[MAIN_TSK_SIZE_64];
uint64_t rf_tsk_stack[RF_TSK_SIZE_64];
uint64_t lpm_tsk_stack[LPM_TSK_SIZE_64];

TCB_t main_tsk_tcb;
TCB_t rf_tsk_tcb;
TCB_t lpm_tsk_tcb;

static osThreadId_t main_tsk_handle;
static const osThreadAttr_t main_tsk_attrs = {
	.name = "main_task",
	.priority = (osPriority_t) osPriorityNormal,
	.cb_mem = &main_tsk_tcb,
	.cb_size = sizeof(TCB_t),
	.stack_mem = main_tsk_stack,
	.stack_size = MAIN_TSK_SIZE_64 * 8
};

static osThreadId_t rf_tsk_handle;
static const osThreadAttr_t rf_tsk_attrs = {
	.name = "rf_task",
	.priority = (osPriority_t) osPriorityHigh,
	.cb_mem = &rf_tsk_tcb,
	.cb_size = sizeof(TCB_t),
	.stack_mem = rf_tsk_stack,
	.stack_size = RF_TSK_SIZE_64 * 8
};

static osThreadId_t lpm_tsk_handle;
static const osThreadAttr_t lpm_tsk_attrs = {
    .name = "lpm_task",
    .priority = (osPriority_t) osPriorityLow,
	.cb_mem = &lpm_tsk_tcb,
	.cb_size = sizeof(TCB_t),
	.stack_mem = lpm_tsk_stack,
    .stack_size = LPM_TSK_SIZE_64 * 8
};

/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	SX1276_InitStruct sx1276_Init = {0};
	System_Init();
	debug_Init();
	GPIO_Init();
	SX1276_Init(&sx1276_Init);

	/* Init scheduler */
	osKernelInitialize();

	/* Create the threads */
	main_tsk_handle = osThreadNew(main_tsk, NULL, &main_tsk_attrs);
	rf_tsk_handle = osThreadNew(RF_tsk, NULL, &rf_tsk_attrs);
	lpm_tsk_handle = osThreadNew(LPM_tsk, NULL, &lpm_tsk_attrs);

	/* Init tasks */
	main_tsk_Init();
	RF_tsk_Init();

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	for (;;);

	return -1;
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{ }

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
