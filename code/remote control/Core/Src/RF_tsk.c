/*
 * RF_tsk.c
 *
 *  Author: AyRobi
 */


#include "RF_tsk.h"

#include "main_tsk.h"
#include "cmsis_os.h"
#include "os_config.h"
#include "cmsis_gcc.h"
#include "debug.h"
#include "defines.h"
#include "config.h"
#include "SX1276.h"

#define INPUT_QUEUE_STORAGE_SIZE (RF_ACTION_QUEUE_SIZE * sizeof(RfInput))

__STATIC_INLINE void do_handshake(void);

MessageQueueCb_t input_queue_cb;
uint8_t input_queue_storage[INPUT_QUEUE_STORAGE_SIZE];

static const osMessageQueueAttr_t input_queue_attr = {
		.name = "RF input queue",
		.attr_bits = 0,
		.cb_mem = &input_queue_cb,
		.cb_size = sizeof(MessageQueueCb_t),
		.mq_mem = input_queue_storage,
		.mq_size = INPUT_QUEUE_STORAGE_SIZE
};

static osMessageQueueId_t input_queue = NULL;
static uint8_t send_buffer[RF_SEND_BUFFER_SIZE];

int RF_tsk_Init(void)
{
	input_queue = osMessageQueueNew(RF_ACTION_QUEUE_SIZE, sizeof(RfInput), &input_queue_attr);

	if (input_queue == NULL)
		return OS_INITIALIZATION_ERROR;

	return OK;
}

osStatus_t RF_put_action(RfAction action)
{
	RfInput input = {
			.action = RF_INPUT_NO_ACTION,
			.data = NULL,
			.len = 0
	};

	switch(action) {
	case RF_ACTION_BELL:
		input.action = RF_INPUT_ACTION_BELL;
		break;
	case RF_ACTION_LOCATE_OFF:
		input.action = RF_INPUT_ACTION_LOCATE_OFF;
		break;
	case RF_ACTION_LOCATE_ON:
		input.action = RF_INPUT_ACTION_LOCATE_ON;
		break;
	case RF_ACTION_GET_LOCATION_STATUS:
		input.action = RF_INPUT_ACTION_GET_LOCATE_STATUS;
	}

	return RF_put_input(&input);
}

osStatus_t RF_put_input(RfInput *input)
{
	debug("Put RF action : %d\n\r", (uint32_t)(input->action));
	return osMessageQueuePut(input_queue, input, 0U, 0U);
}

Location RF_get_last_location(void)
{
	Location loc = {0};
	//TODO add location getter
	return loc;
}

__STATIC_INLINE void do_handshake(void)
{
	send_buffer[0] = RF_HANDSHAKE_PING;
	SX1276_Send(RF_HANDSHAKE_PID, send_buffer, 1);
}

void RF_tsk(void *arg)
{
	RfInput rf_input;
	MainAction action;
	osStatus_t status;

	do_handshake();

	for (;;) {
		status = osMessageQueueGet(input_queue, &rf_input, NULL, osWaitForever);
		if (status != osOK)
			continue;

		action = NO_ACTION;

		switch (rf_input.action) {
		case RF_INPUT_ACTION_HANDSHAKE:
			if (rf_input.len < 2 || rf_input.data[0] != RF_HANDSHAKE_PONG) {
				do_handshake();
				break;
			}

			action = rf_input.data[1] ? LOCATION_ON : LOCATION_OFF;
			main_handshake_done();
			break;
		case RF_INPUT_ACTION_LOCATE_ON:
			send_buffer[0] = 1;
			SX1276_Send(RF_SET_LOCATE_STATUS_PID, send_buffer, 1);
			break;
		case RF_INPUT_ACTION_LOCATE_OFF:
			send_buffer[0] = 0;
			SX1276_Send(RF_SET_LOCATE_STATUS_PID, send_buffer, 1);
			break;
		case RF_INPUT_ACTION_ON_LOCATE_STATUS:
			if (rf_input.len < 1)
				break;

			action = rf_input.data[0] ? LOCATION_ON : LOCATION_OFF;
			break;
		case RF_INPUT_ACTION_GET_LOCATE_STATUS:
			SX1276_Send(RF_GET_LOCATE_STATUS_PID, NULL, 0);
			break;
		case RF_INPUT_ACTION_NEW_LOCATION:
			action = NEW_LOCATION_AVAILABLE;
			//TODO register location
			break;
		case RF_INPUT_ACTION_BELL:
			SX1276_Send(RF_BELL_PID, NULL, 0);
			break;
		case RF_INPUT_ACTION_BELL_DONE:
			action = BELL_DONE;
			break;
		case RF_INPUT_NO_ACTION:
			break;
		}

		if (action != NO_ACTION)
			put_action(&action);
	}
}
