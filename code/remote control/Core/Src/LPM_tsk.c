/*
 * LPM_tsk.c
 *
 *  Author: AyRobi
 */

#include "LPM_tsk.h"

#include "cmsis_os.h"
#include "system.h"
#include "main_tsk.h"
#include "GPIO.h"
#include "sx1276.h"

void LPM_tsk(void *arg)
{
	for (;;) {
		int32_t lc = osKernelLock();

		if (
				GPIO_CanSTOP() &&
				Main_Tsk_CanSTOP() &&
				SX1276_CanSTOP())
			System_DeepSleep();
		else
			System_Sleep();

		osKernelRestoreLock(lc);
	}
}
