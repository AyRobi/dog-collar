/*
 * GPIO.c
 *
 *  Author: AyRobi
 */

#include "GPIO.h"

#include "action.h"
#include "config.h"
#include "cmsis_os.h"
#include "utils.h"
#include "Drivers/GPIO_driver.h"

extern osStatus_t put_action(MainAction *);

typedef struct {
	bool is_released;
	uint32_t last_press;
} UserButton_t;

static UserButton_t bell_btn = {
		.is_released = true,
		.last_press = 0
};

static UserButton_t locate_btn = {
		.is_released = true,
		.last_press = 0
};

__STATIC_INLINE uint32_t get_ms_diff(UserButton_t *btn, uint32_t t)
{
	return t >= btn->last_press ?
				t - btn->last_press :
				osKernelMaxMs() - btn->last_press + t;
}

int GPIO_Init(void)
{
	return GPIO_Driver_Init();
}

void GPIO_Set_BellLED_State(bool is_active)
{
	if (is_active)
		GPIO_Set_LED1();
	else
		GPIO_Reset_LED1();
}

void GPIO_Set_LocateLED_State(bool is_active)
{
	if (is_active)
		GPIO_Set_LED2();
	else
		GPIO_Reset_LED2();
}

bool GPIO_CanSTOP(void)
{
	uint32_t t = osKernelGetMs();

	//TODO add synchronization

	return get_ms_diff(&bell_btn, t) > BTN_PRESS_MS_THRESHOLD &&
			get_ms_diff(&locate_btn, t) > BTN_PRESS_MS_THRESHOLD;
}

static void register_press(UserButton_t *btn, MainAction action)
{
	uint32_t t = osKernelGetMs();

	if (!btn->is_released) {
		btn->last_press = t;
		return;
	}

	uint32_t diff = get_ms_diff(btn, t);

	btn->is_released = false;
	btn->last_press = t;

	if (diff <= BTN_PRESS_MS_THRESHOLD)
		return;

	put_action(&action);
}

static void register_release(UserButton_t *btn)
{
	btn->is_released = true;
	btn->last_press = osKernelGetMs();
}

void On_First_UserButton_Click()
{
	register_press(&bell_btn, BELL);
}

void On_First_UserButton_Release()
{
	register_release(&bell_btn);
}

void On_Second_UserButton_Click()
{
	register_press(&locate_btn, LOCATE);
}

void On_Second_UserButton_Release()
{
	register_release(&locate_btn);
}

