/*
 * main_tsk.c
 *
 *  Author: AyRobi
 */

#include "main_tsk.h"

#include "RF_tsk.h"
#include "cmsis_os.h"
#include "os_config.h"
#include "defines.h"
#include "config.h"
#include "debug.h"
#include "GPIO.h"
#include "system.h"
#include "sx1276.h"

#define ACTION_QUEUE_STORAGE_SIZE ACTION_QUEUE_SIZE * sizeof(MainAction)

MessageQueueCb_t action_queue_cb;
uint8_t action_queue_storage[ACTION_QUEUE_STORAGE_SIZE];

static const osMessageQueueAttr_t action_queue_attr = {
		.name = "main action queue",
		.attr_bits = 0,
		.cb_mem = &action_queue_cb,
		.cb_size = sizeof(MessageQueueCb_t),
		.mq_mem = action_queue_storage,
		.mq_size = ACTION_QUEUE_STORAGE_SIZE
};

static osMessageQueueId_t action_queue = NULL;

static StaticSemaphore_t handshake_sem_cb;
static osSemaphoreId_t handshake_sem = NULL;

const osSemaphoreAttr_t handshake_sem_attr = {
		.name = "handshake_semaphore",
		.attr_bits = 0,
		.cb_mem = &handshake_sem_cb,
		.cb_size = sizeof(StaticSemaphore_t)
};

int main_tsk_Init(void)
{
	action_queue = osMessageQueueNew(ACTION_QUEUE_SIZE, sizeof(MainAction), &action_queue_attr);
	handshake_sem = osSemaphoreNew(1, 0, &handshake_sem_attr);

	if (action_queue == NULL || handshake_sem == NULL)
		return OS_INITIALIZATION_ERROR;

	return OK;
}

void main_handshake_done(void)
{
	osSemaphoreRelease(handshake_sem);
}

void main_tsk(void *arg)
{
	MainAction action;
	Location loc;
	osStatus_t status;

	bool is_location_active = false;

	GPIO_Set_BellLED_State(true);
	GPIO_Set_LocateLED_State(true);

	osSemaphoreAcquire(handshake_sem, osWaitForever);

	GPIO_Set_BellLED_State(false);
	GPIO_Set_LocateLED_State(false);

	for (;;) {
		GPIO_Set_LocateLED_State(is_location_active);

		status = osMessageQueueGet(action_queue, &action, NULL, osWaitForever);
		if (status != osOK)
			continue;

		switch (action) {
		case BELL:
			debug("BELL action received\n\r");
			GPIO_Set_BellLED_State(true);
			RF_put_action(RF_ACTION_BELL);
			break;
		case BELL_DONE:
			debug("BELL action done\n\r");
			GPIO_Set_BellLED_State(false);
			break;
		case LOCATE:
			debug("LOCATE action received\n\r");

			if (is_location_active) {
				RF_put_action(RF_ACTION_LOCATE_OFF);
			} else {
				RF_put_action(RF_ACTION_LOCATE_ON);
			}
			break;
		case LOCATION_ON:
			debug("Location turned ON\n\r");
			is_location_active = true;
			break;
		case LOCATION_OFF:
			debug("Location turned OFF\n\r");
			is_location_active = false;
			break;
		case NEW_LOCATION_AVAILABLE:
			loc = RF_get_last_location();
			debug(
					"New location available : [lat/long/gdop] = [%d, %d, %d]\n\r",
					loc.longitude, loc.latitude, loc.gdop
			);
			is_location_active = true;
			//TODO send to another function for location communication to user (probably bluetooth)
			break;
		case NO_ACTION:
			break;
		}
	}
}

osStatus_t put_action(MainAction *action)
{
	debug("Put main action : %d\n\r", (uint32_t)*action);
	return osMessageQueuePut(action_queue, action, 0U, 0U);
}

bool Main_Tsk_CanSTOP(void)
{
	return !osMessageQueueGetCount(action_queue);
}
