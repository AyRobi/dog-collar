/*
 * action.h
 *
 *  Author: AyRobi
 */

#ifndef INC_ACTION_H_
#define INC_ACTION_H_

#include "stdint.h"

/* --Enumerations-- */

/* Describes user action */
typedef enum {
	NO_ACTION,
	BELL,
	LOCATE,
	LOCATION_ON,
	LOCATION_OFF,
	NEW_LOCATION_AVAILABLE,
	BELL_DONE,
} MainAction;

typedef enum {
	RF_ACTION_LOCATE_ON,
	RF_ACTION_LOCATE_OFF,
	RF_ACTION_GET_LOCATION_STATUS,
	RF_ACTION_BELL,
} RfAction;

typedef enum {
	RF_INPUT_NO_ACTION,
	RF_INPUT_ACTION_HANDSHAKE,
	RF_INPUT_ACTION_LOCATE_ON,
	RF_INPUT_ACTION_LOCATE_OFF,
	RF_INPUT_ACTION_ON_LOCATE_STATUS,
	RF_INPUT_ACTION_GET_LOCATE_STATUS,
	RF_INPUT_ACTION_NEW_LOCATION,
	RF_INPUT_ACTION_BELL,
	RF_INPUT_ACTION_BELL_DONE,
} RfInputAction;

typedef struct {
	uint8_t *data;
	uint32_t len;
	RfInputAction action;
} RfInput;

typedef struct {
	uint32_t latitude;
	uint32_t longitude;
	uint32_t gdop;
} Location;

#endif /* INC_ACTION_H_ */
