/*
 * GPIO.h
 *
 *  Author: AyRobi
 */

#ifndef INC_GPIO_H_
#define INC_GPIO_H_

#include <stdbool.h>

int GPIO_Init(void);

void GPIO_Set_BellLED_State(bool is_active);
void GPIO_Set_LocateLED_State(bool is_active);

bool GPIO_CanSTOP(void);

#endif /* INC_GPIO_H_ */
