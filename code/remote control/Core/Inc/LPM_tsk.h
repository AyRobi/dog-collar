/*
 * LPM_tsk.h
 *
 *  Author: AyRobi
 */

#ifndef INC_LPM_TSK_H_
#define INC_LPM_TSK_H_

void LPM_tsk(void *arg);

#endif /* INC_LPM_TSK_H_ */
