/*
 * GPIO_driver.h
 *
 *  Author: AyRobi
 */

#ifndef INC_DRIVERS_GPIO_DRIVER_H_
#define INC_DRIVERS_GPIO_DRIVER_H_

int GPIO_Driver_Init(void);

void GPIO_Set_LED1(void);
void GPIO_Set_LED2(void);

void GPIO_Reset_LED1(void);
void GPIO_Reset_LED2(void);

#endif /* INC_DRIVERS_GPIO_DRIVER_H_ */
