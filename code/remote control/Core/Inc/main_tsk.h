/*
 * main_tsk.h
 *
 *  Author: AyRobi
 */

#ifndef INC_MAIN_TSK_H_
#define INC_MAIN_TSK_H_

#include <stdbool.h>
#include "cmsis_os.h"
#include "action.h"

int main_tsk_Init(void);
void main_tsk(void *arg);
void main_handshake_done(void);
osStatus_t put_action(MainAction *action);

bool Main_Tsk_CanSTOP(void);

#endif /* INC_MAIN_TSK_H_ */
