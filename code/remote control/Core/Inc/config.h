/*
 * config.h
 *
 *  Author: AyRobi
 */

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

#define DEBUG_BAUD_RATE 				115200
#define DEBUG_BUFF_SIZE 				528
#define DEBUG_LOG

#define BTN_PRESS_MS_THRESHOLD 			60
#define ACTION_QUEUE_SIZE 				4
#define RF_ACTION_QUEUE_SIZE 			8
#define RF_SEND_BUFFER_SIZE				16

#define RF_BELL_PID						((uint8_t) 0x01)
#define RF_BELL_DONE_PID				((uint8_t) 0x02)
#define RF_SET_LOCATE_STATUS_PID		((uint8_t) 0x03)
#define RF_GET_LOCATE_STATUS_PID		((uint8_t) 0x04)
#define RF_ON_LOCATE_STATUS_PID			((uint8_t) 0x05)
#define RF_ON_LOCATION_PID				((uint8_t) 0x06)
#define RF_HANDSHAKE_PID				((uint8_t) 0x55)

#define RF_HANDSHAKE_PING				((uint8_t) 0x11)
#define RF_HANDSHAKE_PONG				((uint8_t) 0x22)

#endif /* INC_CONFIG_H_ */
