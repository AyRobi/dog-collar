/*
 * RF_tsk.h
 *
 *  Author: AyRobi
 */

#ifndef INC_RF_TSK_H_
#define INC_RF_TSK_H_

#include "action.h"
#include "cmsis_os.h"

int RF_tsk_Init(void);
osStatus_t RF_put_action(RfAction action);
osStatus_t RF_put_input(RfInput *input);
Location RF_get_last_location(void);
void RF_tsk(void *arg);

#endif /* INC_RF_TSK_H_ */
