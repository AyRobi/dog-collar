# Dog collar
Dog collar is a simple STM32 based project aiming at locating and beeping your dog.

In its first version, the collar will be able to beep your dog by using a simple remote control. Furthermore, if you have to find your dog, a locate functionality can be activated by pressing a button on the remote control. Then, the dog's location will be displayed on a map, through an Android app and a Bluetooth link. Finally, the communication between the remote control and the collar is achieved by using the LoRa technology, in this way the GNSS based location can be retrieved at 10 kilometers.

*Please note that this project is not yet finished and is built over my free time. That is why the source code and schematics are still in progress.*

## TODO list
The actual tasks list of the project is:
- [x] create a simple debug functionality based on the USART2
- [x] wires user buttons and user LEDs on the remote control
- [x] create a GPIO driver for remote control to get user input from buttons and control LEDS
- [x] create a SPI and TIM drivers for the SX1276 module
- [ ] complete SX1276 driver
- [ ] add encryption capability to LoRa frames
- [x] create the remote control main task to get user inputs and give outputs
- [x] add low power mode (LPM) to the remote control (PWR driver and LPM task)
- [x] define LoRa frames and create a RF task which communicates with the main task (remote control side)
- [x] add LPUART driver in the collar to communicate with the GPS6MV2 module
- [ ] create a driver to translate NMEA sentences
- [ ] add beep functionality to the collar
- [ ] add RF and main tasks to receive LoRa frames from the remote control
- [ ] add low power mode to the collar
- [ ] add Bluetooth capability to remote control
- [ ] create the Android app (Bluetooth link + map display)

## Technologies
The main technologies used in this project are:
* C
* STM32L476 and STM32F401
* ARM Cortex-M4
* STM32 Low Layer library
* FreeRTOS with CMSIS-RTOS v2 interface
* STM32CubeIDE
* SPI
* UART/LPUART
* LoRa (RFM95 module)
* GNSS, Neo-6m (GPS6MV2 module)
* Bluetooth
* Android X
* KiCad

## Schematics
Below you can find the actual version of the minimalist electronic diagram.
![Electronic diagram](./schematics/dog_collar.svg)

## Project structure
Below you can find the main folders:
* `./schematics` contains the KiCad project to design the electronic parts.
* `./code/Common` contains all drivers which are common to Collar and Remote control. Supports STM32F401 and STM32L476 MCUs.
* `./code/collar` contains the source code of the collar.
* `./code/remote control` contains the source code of the remote control.

The source code is divided into three main layers:
* **Hardware level drivers**: those drivers offers a first level of abstraction of hardware peripherals.
* **High-level drivers**: here drivers encapsulate one or more hardware drivers to offer a stable and complete interface to the user.
* **Tasks**: define the highest level. A task interacts with high-level drivers to control its environment or to react to external events. OS specific functionalities, like queues and semaphores, are used to communicate and synchronize tasks.

## Authors
Aymeric Robitaille (@AyRobi)